import { useState } from "react";
import { Tabs, Box } from "@bigcommerce/big-design";
import HtmlCssJs from "../components/HtmlCssJs/HtmlCssJs";
import JsonEditor from "../components/jsonEditor/JsonEditor";
import GraphqlEditor from "../components/graphqlEditor/GraphqlEditor";

const Home = () => {
  const [activeTab, setActiveTab] = useState("tab1");

  const tabs = [
    { id: "tab1", title: "HTML/CSS/JS" },
    { id: "tab2", title: "JSON" },
    { id: "tab3", title: "graphQL" },
  ];
  return (
    <Box marginTop="large" marginLeft="large">
      <Tabs activeTab={activeTab} items={tabs} onTabClick={setActiveTab} />
      <Box marginTop="large">
        {activeTab === "tab1" && <HtmlCssJs />}
        {activeTab === "tab2" && <JsonEditor />}
        {activeTab === "tab3" && <GraphqlEditor />}
      </Box>
    </Box>
  );
};

export default Home;
