import { useState, useEffect } from "react";
import ace from "brace";
import "brace/mode/json";
import "brace/theme/github";
//import { JsonEditor as Editor } from "jsoneditor-react";
import "jsoneditor-react/es/editor.min.css";
import { Box, Button } from "@bigcommerce/big-design";
//import Ajv from "ajv";
import AceEditor from "react-ace";
import "brace/mode/json";
import "brace/theme/solarized_dark";
import "brace/snippets/json";
import "brace/ext/language_tools";
import template from "../../lib/schema.json";
import { EditSession } from "brace";

const JsonEditor = () => {
  const jsonTemplate = JSON.stringify(template, null, 2);
  //const ajv = new Ajv({ allErrors: true, verbose: true });
  const [value, setValue] = useState(jsonTemplate);
  const [annotations, setAnnotations] = useState([]);
  const [isErrors, setIsErrors] = useState(false);

  const getAnnotationsPromise = (value) => {
    const editSession = new EditSession(value);
    editSession.setMode("ace/mode/json");

    return new Promise((resolve) => {
      editSession.on("changeAnnotation", () => {
        const annotations = editSession.getAnnotations();
        editSession.removeAllListeners("changeAnnotation");
        editSession.destroy();
        resolve(annotations);
      });
    });
  };

  const handleChange = async (v) => {
    setValue(v);
    const annotations = await getAnnotationsPromise(v);
    setAnnotations(annotations);
  };
  const hanldeClick = () => {
    console.log(value);
  };

  useEffect(() => {
    if (annotations && annotations.length) {
      annotations.map((annotation) => {
        if (annotation.type === "error") {
          setIsErrors(true);
        } else setIsErrors(false);
      });
    } else setIsErrors(false);
  }, [annotations]);

  return (
    <Box>
      {/* <Editor
        value={json}
        onChange={handleChange}
        ace={ace}
        theme="ace/theme/github"
      /> */}
      <AceEditor
        mode="json"
        theme="solarized_dark"
        name="json-editor"
        fontSize={14}
        width="760px"
        showPrintMargin={true}
        showGutter={true}
        highlightActiveLine={true}
        value={value}
        setOptions={{
          enableBasicAutocompletion: true,
          enableLiveAutocompletion: true,
          enableSnippets: true,
          showLineNumbers: true,
          tabSize: 2,
        }}
        editorProps={{ $blockScrolling: true }}
        onChange={handleChange}
      />
      <Button
        disabled={isErrors}
        marginTop="large"
        type="button"
        onClick={() => hanldeClick()}
      >
        Update
      </Button>
    </Box>
  );
};

export default JsonEditor;
