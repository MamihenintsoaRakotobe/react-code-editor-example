import React, { useState } from "react";
import { GraphQLEditor, LightTheme } from "graphql-editor";
import { Box, Button } from "@bigcommerce/big-design";

const GraphqlEditorComponent = () => {
  const schemas = {
    pizza: `
      type Query{
          pizzas: [Pizza!]
      }
      `,
    pizzaLibrary: `
      type Pizza{
        name:String
      }
      `,
  };
  const [mySchema, setMySchema] = useState({});

  return (
    <div
      style={{
        width: "100%",
        height: "90vh",
        alignSelf: "stretch",
        position: "relative",
      }}
    >
      <GraphQLEditor
        schema={mySchema}
        theme={LightTheme}
        setSchema={(props) => setMySchema({ code: props.code })}
      />

      <Button
        marginTop="large"
        onClick={() => console.log("===> schema", mySchema)}
      >
        Update
      </Button>
    </div>
  );
};

export default GraphqlEditorComponent;
